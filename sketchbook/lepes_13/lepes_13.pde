// Tizenharmadik lépés
// Több, tetszőleges számú gyümölcs is lehessen
// A tömb és a ciklus (iterátor) fogalmának bevezetése

int maxGyumolcsok = 3 ; // Egyszerre hány gyümölcs lehessen a képernyőn

// float x, y;         // a labda középpontja, setup()-ban kapnak értéket, amikor már van vászon! 
float[] arr_x = new float[maxGyumolcsok] ;
float[] arr_y = new float[maxGyumolcsok] ;

// float d;            // a labda mérete
float[] arr_d = new float[maxGyumolcsok];

// float vy=0;         // a labda pillanatnyi sebessége
// A tömböt a setup()-ban majd ki kell nullázni
float[] arr_vy = new float[maxGyumolcsok] ; 

// Az is egy jó ötlet, hogy gyümölcsönként külön g lehetne egy tömbben
// De itt most maradjon egy közös
float g = 0.3;      // gravitációs állandó, tapasztalati érték
color bg = #6C71F2; // a semleges háttérszín itt most már lényeges, a találatok számításához

int p = 0 ;         // pontok száma, kezdetben nulla, lehet negatív is 
String s = "Pontszám: " ;

// PImage eper, gorogdinnye ;  // legyen kétféle gyümölcs

int milyenGyumolcs = 0 ;    // ez megmondja, milyen gyümölcs legyen a következő
// A tömböt a setup()-ban majd ki kell nullázni
int[] arr_milyenGyumolcs = new int[ maxGyumolcsok ] ;
PImage[] arr_gyumolcsKep = new PImage[ maxGyumolcsok ] ; 

float dx = 0 ;      // az x koordinátához adandó érték (előjeles)
float[] arr_dx = new float[ maxGyumolcsok ] ;

void setup () {
  size(800, 600);
  imageMode( CENTER ) ;

  for(int i = 0 ; i<arr_vy.length; i++)
  {
    arr_vy[i] = 0 ;
  }

  for(int i = 0 ; i<arr_milyenGyumolcs.length; i++)
  {
    arr_milyenGyumolcs[i] = 0 ;
  }
  
  for( int i = 0 ; i<arr_milyenGyumolcs.length ; i++)  //<>//
  {
    fruitFactory( i, 0 ) ;
  }
}

void draw () {
  background(bg);
  text( s + p, 5, 20 );

  for (int i=0 ; i<arr_milyenGyumolcs.length;i++)
  {
    image( arr_gyumolcsKep[i], arr_x[i], arr_y[i] ) ;
  }

  // y = y + vy ;    // y+=vy
  for(int i=0 ; i<arr_y.length ; i++) { arr_y[i]+= arr_vy[i] ; }
  
  // vy = vy + g ;   // vy+=g
  for(int i=0 ; i<arr_vy.length ; i++) { arr_vy[i]+= g ; }
  
  // x = max(x+dx , d/2 ) ;
  for(int i=0 ; i<arr_x.length ; i++)
  {
    arr_x[i]= max(arr_x[i]+arr_dx[i], (arr_d[i])/2) ;
  }

  // x = min(x + dx , width-d/2);
  for(int i=0 ; i<arr_x.length ; i++)
  {
    arr_x[i]= min(arr_x[i]+arr_dx[i], width-(arr_d[i])/2) ;
  }

  // hibapont (leesés) vizsgálata
  for(int i=0; i<arr_y.length; i++)
  {
    if ( arr_y[i] > height+200 ) 
    {
      fruitFactory( i, -1 ) ;
    }
  }
}

void mouseMoved () 
{
  // találat vizsgálata
  for( int i=0 ; i<arr_x.length ; i++)
  {
    if (dist(mouseX, mouseY, arr_x[i], arr_y[i]) < arr_d[i]/2) 
    {
      fruitFactory( i, +1 ) ;
    }
  }
}

/**
* Legyárt egy gyümölcsöt a vektor adott pozícióján, és frissíti a pontszámot.
* Paraméterek: 
*  j : a vektor indexe, 0..array.length-1 közötti
*  dp: delta pontszám, 0,-0,+1. Ennyit kell a pontszámhoz adni
*/

void fruitFactory( int j, int dp )
{
  // d=random(50, 100);
  arr_d[j]=random(50,100);
  
  // x=random(d/2, width-d/2);
  arr_x[j]=random(arr_d[j]/2,width-arr_d[j]/2);

  dx = random( -3, 3 );
  arr_dx[j] = random(-3,3);

  // A 200 helyett jó lenne egy random érték.
  // Ne mindig ugyanonnan induljon az összes gyömölcs
  // y = height+200;
  arr_y[j] = height + random(160,240);

  // Ne azonos legyen a kezdősebesség
  // vy = -20;
  arr_vy[j] = random(-24,-16);

  milyenGyumolcs = int(random(1, 2.9)) ; // mert a random() float-ot ad vissza, ezt tovább kell alakítanunk int-té. A >2.0 azért kell, mert az intervallum jobbról nyílt. Lehetne 3 is.
  arr_milyenGyumolcs[j] = int(random(1,2.9)) ;

  // eper         = loadImage("eper.png") ;  // betöltjük a gyümölcsök képeit a ./data könyvtárból
  // gorogdinnye  = loadImage("gorogdinnye.png");

  // eper.resize(int(d), 0) ;  // átméretezzük, mert a netről letöltött fotók nagyon nagyok
  // gorogdinnye.resize(int(d), 0) ;
  
  if(arr_milyenGyumolcs[j] == 1)
  {
    // eper
    arr_gyumolcsKep[j] = loadImage("eper.png") ;
  }
  else if( arr_milyenGyumolcs[j] == 2 )
  {
    // dinnye
    arr_gyumolcsKep[j] = loadImage("gorogdinnye.png") ;
  }  
  else
  {
    arr_gyumolcsKep[j] = loadImage("eper.png") ;
  }
  
  arr_gyumolcsKep[j].resize(int(arr_d[j]), 0) ;

  p += dp ;
}

// FIXME: tippeket átnézni, FIXME-kből ide beemelni, a mostaniakat átnézni és törölni
// - további tippek: 
// - visszapattanjon a canvas faláról, vagy 1. és 4. negyedből (0..width/4, 3*width/4,width) induljon és csak "befelé" eshessen 
// - további konstansok kiemelése: 50 és 100 lehet legkisebbGyumolcs, legnagyobbGyumolcs, 200-at kivenni, stb.