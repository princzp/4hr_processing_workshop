// Hatodik lépés
// Random helyen ugorjon vissza a fel-feldobott labda. Mérete is legyen véletlenszám.

float x,y;      // a labda középpontja, setup()-ban kapnak értéket, amikor már van vászon! 
float d;        // a labda mérete
float vy=0;     // a labda pillanatnyi sebessége 
float g = 0.3;  // gravitációs állandó, tapasztalati érték

void setup () {
    size(800, 600);

    d=random(50,100);
    x=random(d/2,width-d/2);

    y = height+200;
    vy = -20;
}

void draw () {
    background(255);
    fill(#FF00FF);
    stroke(#00FF00);
    ellipse(x, y, d, d);

    y = y + vy ;    // y+=vy
    vy = vy + g ;   // vy+=g

    if( y > height+200 ){
      d = random(50,100);
      vy = -20;

      // Hatodik lepes
      // Random helyen ugorjon vissza.
      x=random(d/2,width-d/2);
    }
}
