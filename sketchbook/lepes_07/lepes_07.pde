// Hetedik lépés
// A gyümölcs eltalálása. Ha benne van az egér a körben, akkor változzon meg a színe.
// Mentorok nézzék meg, hogy tutira ne a setup-ba menjen a kód amit itt írunk.

float x,y;      // a labda középpontja, setup()-ban kapnak értéket, amikor már van vászon! 
float d;        // a labda mérete
float vy=0;     // a labda pillanatnyi sebessége 
float g = 0.3;  // gravitációs állandó, tapasztalati érték

void setup () {
    size(800, 600);

    d=random(50,100);
    x=random(d/2,width-d/2);

    y = height+200;
    vy = -20;
}

void draw () {
    background(255);
    // ezt most kivesszük, pont hogy a szín fog változni aszerint, hogy eltalálta-e az egérrel
    // fill(#FF00FF);

    if (dist(mouseX, mouseY, x, y) < d/2)
    {
      fill(#FF0000); // pirosra vált, ha eltalálták az egérrel
    } 
    else 
    {
      fill(#6C71F2);
    }

    stroke(#00FF00);
    ellipse(x, y, d, d);

    y = y + vy ;    // y+=vy
    vy = vy + g ;   // vy+=g

    if( y > height+200 ){
      d = random(50,100);
      vy = -20;

      x=random(d/2,width-d/2);
    }
}
