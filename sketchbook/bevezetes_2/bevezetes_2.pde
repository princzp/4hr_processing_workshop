// Ez megjegyzés, a fordítóprogram el sem olvassa azokat a sorokat, amelyek így kezdődnek: //
// Mire jó? Írhatsz magyarázatot egy másik programozónak, aki majd a programodat olvassa, módosítja.
// Jegyzetelhetsz is így. Akár magyarul is.
// Még egy dologra hasznos: ha egy érvényes utasítás elé // jelet teszel, azzal ideiglenesen eltávolítod a programból, de könnyű lesz majd visszatenni.
// Ez hibakeresésnél, próbálgatásnál lesz nagyon hasznos. Mindjárt látni fogod.

/**
 * Többsoros megjegyzés példa. 
 * 
 * Ilyennel is gyakran fogsz találkozni.
 *  
 */

// A setup() a program indulásakor egyszer fut le
void setup() {
  // Legyen a vászon 200x200 pixel méretű.
  size(200, 200);
    
  // background(204);
}

// A draw() a setup() után, másodpercenként sokszor fut le, és frissíti a képernyőt. 
// Akár a végtelenségig is futhat, amíg kézzel le nem állítod a programot.
void draw() {
  background(204);
  
  // A line() egy egyenes vonallal összeköt két pontot. Itt most a vászon közepét az egérkurzorral. A pontok x és y koordinátával határozottak, ebben a sorrendben.
  line(width/2, height/2, mouseX, mouseY); //<>//
}

// Találós kérdés, feladat: a background() lényegében törli a vásznat. Hová kell tenni? A setup()-ba vagy a draw()-ba?
// Próbáld ki mindkét helyen és hasonlítsd össze a programjaidat!
// Használd a // megjegyzés jelet az utasítás ideiglenes eltávolítására!

// Hányszor frissíti a draw() a vásznat másodpercenként?
// Keresd ki a Processing referenciából! (Help => Reference)
// Állits be a setup()-ban különböző (30,10,5) képfrissitési értékeket és próbálgasd a programodat!