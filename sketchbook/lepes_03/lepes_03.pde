// Harmadik lépés
// Random helyről essen le a labda.
// Az x tengelyen indulhasson bárhonnan, akár a vászonról lelógva.
// Az y tengelyen a vászon felső feléről induljon, akár a vászonról lelógva.

float x,y;

void setup () {
    size(800, 600);
    x=random(0,width);
    y=random(0,height/2);
}

void draw () {
    background(255);
    fill(#FF00FF);
    stroke(#00FF00);
    ellipse(x, y, 100, 100);
 
    y = min( ++y , height-50 ) ;
    
    // Extra: miért 50? Mert d/2.
    // Kezdje el zavarni a túl sok behegesztett 100 és 50 konstans.
    // Vezessen be új változót d-nek, kapjon kezdőértéket a setup()-ban.
    // A képletben ekkor persze height-d/2 szerepel majd.
    
    // Extra: a labda minden pontja mindig a vászonra essen, sose lógjon le róla.
    // Ekkor setup()-ban:
    // x=random(50,width-50);
    // y=random(50,height/2);
    // de már nem konstansokkal, hanem képlettel ott is.
}