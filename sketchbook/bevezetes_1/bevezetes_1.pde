// Nyit egy 800x600 pixel méretű üres vásznat.
size(800, 600);

// Ezek sorrendje rétegezést is jelent, figyeld meg!

// egy satírozott kör
fill(#FF00FF);
stroke(#00FF00);
ellipse(width/2, height/2, 100, 100);
// idáig tart a kör

// egy üres téglalap
noFill();
stroke(#000000);
rect(width/2, height/2, 100, 100);
// idáig tart a téglalap

// A két síkidom átlapolódik
// Cseréljük meg őket, nézzük meg, mi történik.