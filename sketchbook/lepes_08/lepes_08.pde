// Nyolcadik, befejező lépés
// Kimozgatni az előző lépésben fejlesztett elágazást a mousePressed() eseménykezelőbe.
// Már le sem esik, ha eltalálták. Kattintáskor azonnal jön az új gyümölcs.

float x,y;      // a labda középpontja, setup()-ban kapnak értéket, amikor már van vászon! 
float d;        // a labda mérete
float vy=0;     // a labda pillanatnyi sebessége 
float g = 0.3;  // gravitációs állandó, tapasztalati érték
color bg = #6C71F2; // 

void setup () {
    size(800, 600);

    d=random(50,100);
    x=random(d/2,width-d/2);

    y = height+200;
    vy = -20;
}

void draw () {
    background(bg);
    fill(#CEC628);
    stroke(#00FF00);
    ellipse(x, y, d, d);

    y = y + vy ;    // y+=vy
    vy = vy + g ;   // vy+=g

    if( y > height+200 ){
      d = random(50,100);
      vy = -20;

      x=random(d/2,width-d/2);
    }
}

void mousePressed () {
  if (dist(mouseX, mouseY, x, y) < d/2) 
  {
    y = height+200;
    d = random(50,100);
    vy = -20;

    x=random(d/2,width-d/2);
  }
}

// Extra:
// - Pontozás: számolni az eltalált gyümölcsöket, esetleg a lehullókért meg levonni, és akkor lehessen előjeles is.
// - Képek, igazi gyümölcsök. Ekkor a találat programozása nem triviális. Ötlet: minden gyümölcs színétől különböző háttér és a get()-tel vizsgálni az adott pixel színét.
// - x-beni mozgás, vízszintes hajítás

