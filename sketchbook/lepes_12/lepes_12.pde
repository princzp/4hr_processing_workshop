// Tizenkettedik lépés
// Extra ötletek a 8. lépés végéről:
// - KÉSZ: Pontozás: számolni az eltalált gyümölcsöket, esetleg a lehullókért meg levonni, és akkor lehessen előjeles is.
// - KËSZ: Képek, igazi gyümölcsök. Ekkor a találat programozása nem triviális. Ötlet: minden gyümölcs színétől különböző háttér és a get()-tel vizsgálni az adott pixel színét.
// - KÉSZ: x-beni mozgás, vízszintes hajítás. 
//   A mousePressed()-et cseréljük mouseMoved()-re, hogy jobban hasonlítson az eredeti játékra, ahol elég csak hadonászni...
//
// - MOST: Refaktorálás. Vegyük észre, héromszor gyártunk gyümölcsöt. 
//   Ezeket emeljük ki egy önálló függvénybe, és hívjuk meg az eddigi helyeikről

float x, y;         // a labda középpontja, setup()-ban kapnak értéket, amikor már van vászon! 
float d;            // a labda mérete
float vy=0;         // a labda pillanatnyi sebessége 
float g = 0.3;      // gravitációs állandó, tapasztalati érték
color bg = #6C71F2; // a semleges háttérszín itt most már lényeges, a találatok számításához

int p = 0 ;         // pontok száma, kezdetben nulla, lehet negatív is 
String s = "Pontszám: " ;

PImage eper, gorogdinnye ;  // legyen kétféle gyümölcs
int milyengyumolcs = 0 ;    // ez megmondja, milyen gyümölcs legyen a következő

float dx = 0 ;      // az x koordinátához adandó érték (előjeles)

void setup () {
  size(800, 600);

  /* 1 db új gyümölcs legyártása
  // A pontszám nem változik, az olyan, mintha nullát adnánk hozzá
  d=random(50, 100);
  x=random(d/2, width-d/2);
  dx = random( -3, 3 );

  y = height+200;
  vy = -20;

  eper         = loadImage("eper.png") ;  // betöltjük a gyümölcsök képeit a ./data könyvtárból
  gorogdinnye  = loadImage("gorogdinnye.png");

  eper.resize(int(d), 0) ;  // átméretezzük, mert a netről letöltött fotók nagyon nagyok
  gorogdinnye.resize(int(d), 0) ;

  milyengyumolcs = int(random(1, 2.9)) ; // mert a random() float-ot ad vissza, ezt tovább kell alakítanunk int-té. A >2.0 azért kell, mert az intervallum jobbról nyílt. Lehetne 3 is.
  */
  
  fruitFactory( 0 ) ;
}

void draw () {
  background(bg);
  text( s + p, 5, 20 );

  // Az ellipszis helyett igazi gyümölcs jön
  // fill(#CEC628);
  // stroke(#00FF00);
  // ellipse(x, y, d, d); // végleges kódból majd kivenni, de tesztelni nagyon jó
  
  imageMode( CENTER ) ; // Ekkor a gyümölcsöt az eddigi körbe tesszük, éa a találat vizsgálata akár maradhat is

  if ( milyengyumolcs == 1)
  { 
    image(eper, x, y);
  } 
  else if ( milyengyumolcs == 2)
  {
    image(gorogdinnye, x, y);
  } 
  else // ez elvileg elő sem fordulhat
  {
    image(eper, x, y);
  }

  // Ha van idő a switch-et megbeszélni
  //switch(milyengyumolcs)
  //{
  //case 1:
  //  image(eper, x, y);
  //  break ;
  //case 2:
  //  image(gorogdinnye, x, y);
  //  break ;
  //default:
  //   image(eper, x, y);
  //}

  y = y + vy ;    // y+=vy
  vy = vy + g ;   // vy+=g
  
  x = max(x+dx , d/2 ) ;
  x = min(x + dx , width-d/2);

  if ( y > height+200 ) {
    fruitFactory( -1 ) ;
    
    /*
    d = random(50, 100);
    vy = -20;
    p = p - 1 ; // leesett, levonunk egy pontot

    x=random(d/2, width-d/2);
    dx = random( -3, 3 );
    milyengyumolcs = int(random(1, 2.9)) ; // megint ki kell találni, milyen gyümölcs legyen a következő
    eper         = loadImage("eper.png") ; // inkább töltsük be újból, az átméretezés miatt
    gorogdinnye  = loadImage("gorogdinnye.png");
    eper.resize(int(d), 0) ;  // átméretezzük, mert a netről letöltött fotók nagyon nagyok
    gorogdinnye.resize(int(d), 0) ;
    */
  }
}

//void mousePressed () {
void mouseMoved () {
  if (dist(mouseX, mouseY, x, y) < d/2) 
  {
    fruitFactory( +1 ) ;
    
    /*
    y = height+200;
    d = random(50, 100);
    vy = -20;
    p = p + 1 ; // eltalálta, kap egy pontot

    x=random(d/2, width-d/2);
    dx = random( -3, 3 );
    milyengyumolcs = int(random(1, 2.9)) ; // megint ki kell találni, milyen gyümölcs legyen a következő
    eper         = loadImage("eper.png") ; // inkább töltsük be újból, az átméretezés miatt
    gorogdinnye  = loadImage("gorogdinnye.png");
    eper.resize(int(d), 0) ;  // átméretezzük, mert a netről letöltött fotók nagyon nagyok
    gorogdinnye.resize(int(d), 0) ;
    */
  }
}

void fruitFactory( int dp )
{
  p += dp ;
  
  d=random(50, 100);
  x=random(d/2, width-d/2);
  dx = random( -3, 3 );

  y = height+200;
  vy = -20;

  eper         = loadImage("eper.png") ;  // betöltjük a gyümölcsök képeit a ./data könyvtárból
  gorogdinnye  = loadImage("gorogdinnye.png");

  eper.resize(int(d), 0) ;  // átméretezzük, mert a netről letöltött fotók nagyon nagyok
  gorogdinnye.resize(int(d), 0) ;

  milyengyumolcs = int(random(1, 2.9)) ; // mert a random() float-ot ad vissza, ezt tovább kell alakítanunk int-té. A >2.0 azért kell, mert az intervallum jobbról nyílt. Lehetne 3 is.
}

// - további tippek: 
// - visszapattanjon a canvas faláról, vagy 1. és 4. negyedből (0..width/4, 3*width/4,width) induljon és csak "befelé" eshessen 
// - további konstansok kiemelése: 50 és 100 lehet legkisebbGyumolcs, legnagyobbGyumolcs, 200-at kivenni, stb.