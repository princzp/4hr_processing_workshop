// Negyedik lépés
// Gyorsuljon a függőlegesen leeső (később: feldobott) labda. 
// Oktatónak: kis beszéd a fizikáról. Pillanatnyi sebesség, gyorsulás fogalmának bevezetése.
// A labda mérete itt már legyen kiemelve, ne nagyon legyenek behegesztett konstansok a programban.

float x,y;      // a labda középpontja, setup()-ban kapnak értéket, amikor már van vászon! 

float d=100;    // a labda mérete
float vy=0;     // a labda pillanatnyi sebessége 
float g = 0.3;  // gravitációs állandó, tapasztalati érték

void setup () {
    size(800, 600);
    x=random(d/2,width-d/2);

    // alapfeladat
    y=random(d/2,height/2);

    // Nem extra. Ide el kell jutni, egy kis fizikai alapozás után
    // Legjobb ha maguktól jönnek rá a negatív sebességre
    // Függőleges hajítás és visszahullás pusztán a paraméterek változtatásával
    // y = height+200;
    // vy = -20;
}

void draw () {
    background(255);
    fill(#FF00FF);
    stroke(#00FF00);
    ellipse(x, y, d, d);

    y = y + vy ;    // y+=vy
    vy = vy + g ;   // vy+=g

    y = min (y, height-(d/2)) ;

}
