// Második lépés
// Labda leesik egyenletes sebességgel. Változók bevezetése.

float y;

void setup () {
    size(800, 600);
    y=0;
}

void draw () {
    background(255);
    fill(#FF00FF);
    stroke(#00FF00);
    ellipse(width/2, y, 100, 100);
    
    // Alapfeladat: menjen ki alul a végtelenbe
    y=y+1; 
    // Extra: ezt a jelölést esetleg elmondani: y=y+1 helyett ++y vagy y++;
    
    // Diagnosztikai üzenetek a konzolra
    println(y);
    
    // Bonyolultabb feladat: most alul álljon meg a földön
    // y = min( ++y , height-50 ) ;
    
    // Extra: miért 50? Mert d/2.
    // Vezessen be új változót d-nek, kapjon kezdőértéket a setup()-ban.
    // A képletben ekkor height-d/2 szerepel majd.
}