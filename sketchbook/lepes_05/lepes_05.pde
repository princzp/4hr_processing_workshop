// Ötödik lépés
// Fel- feldobott labda. Azaz leesés után mintha megint feldobná "valaki".
// Az elágazás fogalmának bevezetése.

float x,y;      // a labda középpontja, setup()-ban kapnak értéket, amikor már van vászon! 

float d=100;    // a labda mérete
float vy=0;     // a labda pillanatnyi sebessége 
float g = 0.3;  // gravitációs állandó, tapasztalati érték

void setup () {
    size(800, 600);
    x=random(d/2,width-d/2);

    y = height+200;
    vy = -20;
}

void draw () {
    background(255);
    fill(#FF00FF);
    stroke(#00FF00);
    ellipse(x, y, d, d);

    y = y + vy ;    // y+=vy
    vy = vy + g ;   // vy+=g

    // Ezt finomítjuk most
    // y=min(y, height-(d/2)) ;

    if( y > height+200 ){
      vy = -20;
    }

    // Extra: if és különösen else, többágú if a referenciában
}
