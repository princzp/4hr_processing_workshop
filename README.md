Teach the very basics of programming to teenagers.
(Age 15-18, and the content is geared towards girls, specifically, but should work for boys as well.)

Implement a simple variant of fruit ninja game within 4 hours in Processing, in 8 gradual steps.
The math and physics apparatus is simplified on purpose and fits the targeted age group:
  - very basic coordinate geometry concepts: pixel, point, ellipse, circle
  - and simple concepts from physics: projectile motion, current speed,  acceleration, gravity.

There should be no new math/physics concepts compared to their regular school curricula.

Focus is on programming instead, but with the instant reward of visual feedback as well as building mathematical/physical mental model of the problem at hand. 
Kids will learn some programming concepts, i.e.:
  - variables and value assigments to variables, operators and comparison
  - sequence of instructions
  - simple if..else branching
  - generate random numbers, invoke library functions in general
  - refactoring, move code into a newly introduced function, removing parts of code temporarily by commenting it out
  - basic debug and troubleshooting

Arrays, matrices and all kinds of loops are intentionally left out. The implicit canvas refreshing loop of the processing environment is used instead in an intuitive way that fits their age.

The language of the comments in the code is Hungarian for now.
